(function () {
    angular.module('App', [])
    angular.module('App').directive('tableResponsive', ['$log', tableResponsive]);

    function tableResponsive($log) {
        $log.debug('tableResponsive::init');
        return {
            restrict: 'A',
            link: function (scope, element) {
                $log.debug('tableResponsive::link');

                // Monitorea la cantidad de elementos de la tabla.
                scope.$watch(function () {
                    return element.prop('rows').length;
                },
                    function (newValue) {
                        var $th = element.find('th');
                        angular.forEach($th, function (itm, idx) {
                            var $headEl = angular.element(itm);
                            var text = $headEl.text().trim();
                            var selector = 'td:nth-child(' + (idx + 1) + ')';
                            var el = element[0];
                            var tds = el.querySelectorAll(selector);
                            angular.forEach(tds, function (item) {
                                var $el = angular.element(item);
                                if (text.length) {
                                    $el.attr('data-th', text + ': ');
                                } else {
                                    $el.attr('data-th', text);
                                }
                            });
                        });
                    });
            }
        }
    }
})();